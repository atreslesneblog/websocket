'use strict';

const SocketIO = require('socket.io');
const http = new require('http');
const serve = require('node-static');

const statics = new serve.Server('../../client/socketio');
const staticServer = http.createServer((req, res) => statics.serve(req, res));
const io = new SocketIO();

io.on('connection', (socket) => {
    socket.on('message', (message) => io.emit('message', message));
});

staticServer.listen(process.env.PORT || 8080, process.env.IP || '0.0.0.0');
io.attach(8081);
