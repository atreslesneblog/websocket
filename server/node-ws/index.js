'use strict';

const WebSocket = new require('ws');
const http = new require('http');
const serve = require('node-static');
const events = require('events');

const dispatcher = new events.EventEmitter();
const statics = new serve.Server('../../client/embedded');
const server = http.createServer((req, res) => statics.serve(req, res));

const socket = new WebSocket.Server({
    port: 8081,
    host: process.env.IP || '0.0.0.0'
});

socket.on('connection', function (ws) {
    let onMessage = (message) => ws.send(message);
    dispatcher.on('message', onMessage);

    ws.on('message', (message) => dispatcher.emit('message', message));
    ws.on('close', () => dispatcher.removeListener('message', onMessage));
});

server.listen(process.env.PORT || 8080, process.env.IP || '0.0.0.0');
