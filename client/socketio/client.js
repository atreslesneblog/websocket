var template = '<i class="comment icon"></i><div class="content"><div class="header">USERNAME <span class="ui tag mini label">DATE</span></div><div class="description">MESSAGE</div></div>';

function ChatClient(url) {
    this.form = document.forms['sendform'];
    this.messages = document.getElementById('messages');
    this.container = document.getElementById('container');
    this.socket = io(url);

    this.form.message.onkeyup = function () {
        if (this.form.message.value && /\bdisabled\b/.test(this.form.button.className)) {
            this.form.button.className = this.form.button.className.replace(/disabled/, '');
        } else if (!this.form.message.value && !/\bdisabled\b/.test(this.form.button.className)) {
            this.form.button.className += ' disabled';
        }
    }.bind(this);

    this.form.onsubmit = function () {
        if (!/\bdisabled\b/.test(this.form.button.className)) {
            this.send(this.form.message.value, this.form.author.value);
        }
        return false;
    }.bind(this);

    this.socket.on('message', function (msg) {
        var data = JSON.parse(msg);
        this.displayMessage(data.message, data.author);
    }.bind(this));
}

ChatClient.prototype.send = function (message, author) {
    this.form.message.value = '';
    this.form.button.className += ' disabled';

    this.socket.emit('message', JSON.stringify({
        message: message,
        author: author || 'Anonymous'
    }));
};

ChatClient.prototype.dateFormat = function (date) {
    function prependZero(value) {
        return (parseInt(value) < 10) ? '0' + value : value;
    }
    return [date.getHours(), date.getMinutes(), date.getSeconds()].map(function (v) {
        return prependZero(v);
    }).join(':');
};

ChatClient.prototype.displayMessage = function (message, author) {
    var el = document.createElement('div');
    el.className = 'item';
    el.innerHTML = template.replace(/USERNAME/, author).replace(/DATE/, this.dateFormat(new Date)).replace(/MESSAGE/, message);
    this.messages.appendChild(el);
    this.container.scrollTop = this.container.scrollHeight;
};
